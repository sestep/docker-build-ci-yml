# Docker Build CI YAML

A GitLab CI YAML [file][include] using smart Docker caching to build and tag an
image for every branch and commit.
```yml
include:
  - project: 'sestep/docker-build-ci-yml'
    file: 'gitlab-ci.yml'
```

---

[include]: https://docs.gitlab.com/ee/ci/yaml/#includefile
